<?php

/*
 * This file is part of qerana
 * Copyright (C) 2020  tyrion.jackson@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\welcome\controller;
use Qerapp\qbasic\model\module\ModuleService;


/**
 * *****************************************************************************
 * Description of WelcomeController
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class WelcomeController
{

    protected
            $_Welcome;

    public function __construct()
    {
        
    }

    public function index()
    {

        //check if module qaccess is installed
        $ModuleService = new ModuleService();
        $ModuleService->module_name = 'qaccess';
        $Qaccess = $ModuleService->getModule();
        
        //check session
        $Session = new \Qerana\core\QSession();
        $tpl_load = ($_SESSION['Q_id_user']) ? 'index' : 'welcome';
        
        \Qerana\core\View::showView($tpl_load,['Qaccess'=>$Qaccess]);
    }

}
