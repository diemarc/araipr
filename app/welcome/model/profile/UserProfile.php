<?php


/*
 * This file is part of qerana core
 * Copyright (C) 2020  tyrion.jackson@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\welcome\model\profile;

use Qerapp\qaccess\model\profile\profiles\QProfileInterface;

class ComercialProfile implements QProfileInterface {

    
    
    
    public function buildProfile() {
        
        
        // write code
        
    }
    

}
