
/**
 * -----------------------------------------------------------------------------
 * run the data json
 * -----------------------------------------------------------------------------
 * @param {type} ofset
 * @param {type} url_filter
 * @param {type} callbackFunction
 * @returns {undefined}
 */
function getDataJson(url_json, callbackFunction) {


    // draw a loader
    drawLoader();

    // show loader
    $('#data-loader').css('display', 'block');
    $('.df-data').hide();

    // get json data
    $.getJSON(url_json, function (data) {

        // hide the loader
        $('#data-loader').css('display', 'none');
        $('.df-data').show();

        // total record
        $('#total').html(data.length);
        

        // assign to data_filtered var, to procees in callbackfucntion
        data_response = data;

        // run the callback function
        var func = new Function(callbackFunction);
        func();


    });

}





/**
 * -----------------------------------------------------------------------------
 * draw a basic loader
 * -----------------------------------------------------------------------------
 * @returns {undefined}
 */
function drawLoader() {

    var html_loader = '<div style="display:none" class="text-default" align="center" id="data-loader">';
    html_loader += '<i class="fa fa-spinner fa-spin fa-4x"></i>';
    html_loader += '</div>';

    $('#loader').html(html_loader);

}
