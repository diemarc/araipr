<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit7b29d0bd947f7c3a7adc60b099f8d08b
{
    public static $files = array (
        '3109cb1a231dcd04bee1f9f620d46975' => __DIR__ . '/..' . '/paragonie/sodium_compat/autoload.php',
    );

    public static $prefixLengthsPsr4 = array (
        'h' => 
        array (
            'helpers\\' => 8,
        ),
        'c' => 
        array (
            'config\\' => 7,
        ),
        'a' => 
        array (
            'app\\' => 4,
        ),
        'Q' => 
        array (
            'Qerapp\\' => 7,
            'Qerana\\' => 7,
            'QException\\' => 11,
        ),
        'P' => 
        array (
            'Psr\\Log\\' => 8,
            'ParagonIE\\HiddenString\\' => 23,
            'ParagonIE\\Halite\\' => 17,
            'ParagonIE\\ConstantTime\\' => 23,
        ),
        'M' => 
        array (
            'Monolog\\' => 8,
        ),
        'C' => 
        array (
            'Cliqer\\' => 7,
        ),
        'A' => 
        array (
            'Ada\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'helpers\\' => 
        array (
            0 => __DIR__ . '/../..' . '/helpers',
        ),
        'config\\' => 
        array (
            0 => __DIR__ . '/../..' . '/config',
        ),
        'app\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
        'Qerapp\\' => 
        array (
            0 => __DIR__ . '/../..' . '/qerapps',
        ),
        'Qerana\\' => 
        array (
            0 => __DIR__ . '/../..' . '/qerana',
        ),
        'QException\\' => 
        array (
            0 => __DIR__ . '/../..' . '/qerana/exception',
        ),
        'Psr\\Log\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/log/Psr/Log',
        ),
        'ParagonIE\\HiddenString\\' => 
        array (
            0 => __DIR__ . '/..' . '/paragonie/hidden-string/src',
        ),
        'ParagonIE\\Halite\\' => 
        array (
            0 => __DIR__ . '/..' . '/paragonie/halite/src',
        ),
        'ParagonIE\\ConstantTime\\' => 
        array (
            0 => __DIR__ . '/..' . '/paragonie/constant_time_encoding/src',
        ),
        'Monolog\\' => 
        array (
            0 => __DIR__ . '/..' . '/monolog/monolog/src/Monolog',
        ),
        'Cliqer\\' => 
        array (
            0 => __DIR__ . '/../..' . '/qerana/cliqer',
        ),
        'Ada\\' => 
        array (
            0 => __DIR__ . '/../..' . '/qerana/ada',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit7b29d0bd947f7c3a7adc60b099f8d08b::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit7b29d0bd947f7c3a7adc60b099f8d08b::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
